<?php

return [
    'api_key' => env('FLICKR_API_KEY', '10d45badcee01e477e4d56b5f285fda3'),

    'route' => env('FLICKR_MAIN_ROUTE', 'https://api.flickr.com/services/rest/'),

    'format' => 'json',

    'nojsoncallback' => 1,

    'photo_link' => env('FLICKR_PHOTO_LINK', 'https://farm5.staticflickr.com')
];
