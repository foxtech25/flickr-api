<?php

namespace App\Adapter;

use GuzzleHttp\Client;

/**
 * Class FlickrAdapter
 * @package App\Adapter
 */
class FlickrAdapter
{
    const STATUS_OK = 'ok';

    /**
     * @var Client
     */
    private $client;

    /**
     * FlickrAdapter constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param array $data
     * @return string
     */
    public function run(array $data): string
    {
        return $this->client->get(
            config('flickr.route'), [
                'query' => array_merge([
                    'api_key'        => config('flickr.api_key'),
                    'format'         => config('flickr.format'),
                    'nojsoncallback' => config('flickr.nojsoncallback')
                ], $data)
        ])
            ->getBody()
            ->getContents();
    }
}