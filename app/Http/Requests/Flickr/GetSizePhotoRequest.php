<?php

namespace App\Http\Requests\Flickr;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ListRecentPhotosRequest
 * @package App\Http\Requests\Flickr
 */
class GetSizePhotoRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|integer|min:1',
        ];
    }

    /**
     * @return array
     */
    public function all()
    {
        $data = parent::all();
        $data['id'] = (int) $this->route('id');

        return $data;
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}