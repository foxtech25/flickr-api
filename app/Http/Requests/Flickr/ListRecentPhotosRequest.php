<?php

namespace App\Http\Requests\Flickr;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ListRecentPhotosRequest
 * @package App\Http\Requests\Flickr
 */
class ListRecentPhotosRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'page'  => 'integer|min:1',
            'limit' => 'integer|min:1|max:50',
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}