<?php

namespace App\Http\Controllers;

use App\Http\Requests\Flickr\GetSizePhotoRequest;
use App\Http\Requests\Flickr\ListRecentPhotosRequest;
use App\Port\Containers\Actions\Flickr\GetSizePhotoAction;
use App\Port\Containers\Actions\Flickr\ListRecentPhotosAction;

/**
 * Class FlickrController
 * @package App\Http\Controllers
 */
class FlickrController extends Controller
{
    /**
     * @param ListRecentPhotosRequest $request
     * @param ListRecentPhotosAction $action
     * @return array
     */
    public function listRecentPhotos(ListRecentPhotosRequest $request, ListRecentPhotosAction $action)
    {
        return $action->run($request->all());
    }

    /**
     * @param GetSizePhotoRequest $request
     * @param GetSizePhotoAction $action
     * @return array|mixed
     */
    public function getSizePhoto(GetSizePhotoRequest $request, GetSizePhotoAction $action)
    {
        return $action->run($request->all());
    }
}
