<?php

namespace App\Port\Containers\Tasks\Flickr;

use App\Adapter\FlickrAdapter;
use App\Port\Contracts\Tasks\Task;

/**
 * Class ListRecentPhotosTask
 * @package App\Port\Containers\Actions\Flickr
 */
class GetSizePhotoTask implements Task
{
    const METHOD_GET_SIZE_PHOTO = 'flickr.photos.getSizes';

    /**
     * @var FlickrAdapter
     */
    private $adapter;

    /**
     * ListRecentPhotosTask constructor.
     * @param FlickrAdapter $adapter
     */
    public function __construct(FlickrAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function run(array $data): array
    {
        $sizePhoto = json_decode(
            $this->adapter->run([
                'photo_id' => $data['id'],
                'method'   => self::METHOD_GET_SIZE_PHOTO
            ])
        );

        if ($sizePhoto->stat != FlickrAdapter::STATUS_OK) {
            return [
                'error' => $sizePhoto
            ];
        }

        return [
            'photo_size' => $sizePhoto->sizes->size,
        ];
    }
}