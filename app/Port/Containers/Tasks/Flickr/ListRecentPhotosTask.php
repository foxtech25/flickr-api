<?php

namespace App\Port\Containers\Tasks\Flickr;

use App\Adapter\FlickrAdapter;
use App\Port\Contracts\Tasks\Task;

/**
 * Class ListRecentPhotosTask
 * @package App\Port\Containers\Actions\Flickr
 */
class ListRecentPhotosTask implements Task
{
    const METHOD_LIST_RECENT_PHOTO = 'flickr.photos.getRecent';
    const DEFAULT_LIMIT = 10;
    const DEFAULT_PAGE = 1;

    /**
     * @var FlickrAdapter
     */
    private $adapter;

    /**
     * ListRecentPhotosTask constructor.
     * @param FlickrAdapter $adapter
     */
    public function __construct(FlickrAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function run(array $data): array
    {
        $limit = $data['limit'] ?? self::DEFAULT_LIMIT;
        $page = $data['page'] ?? self::DEFAULT_PAGE;

        $recentPhotos = json_decode(
            $this->adapter->run([
                'per_page' => $limit,
                'page'     => $page,
                'method'   => self::METHOD_LIST_RECENT_PHOTO
            ])
        );

        return [
            'recent_photo' => $recentPhotos,
            'page'  => $page,
            'limit' => $limit,
        ];
    }
}