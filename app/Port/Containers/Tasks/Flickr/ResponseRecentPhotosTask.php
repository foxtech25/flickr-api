<?php

namespace App\Port\Containers\Tasks\Flickr;

use App\Adapter\FlickrAdapter;
use App\Port\Contracts\Tasks\Task;

/**
 * Class ResponseRecentPhotosTask
 * @package App\Port\Containers\Tasks\Flickr
 */
class ResponseRecentPhotosTask implements Task
{
    const FORMAT_PHOTO = 'jpg';

    /**
     * @param array $data
     * @return array
     */
    public function run(array $data): array
    {
        if ($data['recent_photo']->stat != FlickrAdapter::STATUS_OK) {
            return [
                'error' => $data['recent_photo']
            ];
        }

        foreach ($data['recent_photo']->photos->photo as $photo) {
            $photo->photo = config('flickr.photo_link') . '/'
                . $photo->server . '/'
                . $photo->id . '_'
                . $photo->secret . '.'
                . self::FORMAT_PHOTO;
        }

        return [
            'photos'        => $data['recent_photo']->photos->photo,
            'countAllPosts' => $data['recent_photo']->photos->total,
            'countInPage'   => $data['recent_photo']->photos->perpage,
            'nextPage'      => ($data['recent_photo']->photos->perpage == $data['limit'])
                                ? route('list.recent.photo') . '?page=' . ((int) $data['page'] + 1)
                                : null
        ];
    }
}