<?php

namespace App\Port\Containers\Actions\Flickr;

use App\Port\Containers\Tasks\Flickr\GetSizePhotoTask;
use App\Port\Contracts\Actions\Action;

/**
 * Class GetSizePhotoAction
 * @package App\Port\Containers\Actions\Flickr
 */
class GetSizePhotoAction implements Action
{
    /**
     * @var GetSizePhotoTask
     */
    private $getSizePhotoTask;

    /**
     * GetSizePhotoAction constructor.
     * @param GetSizePhotoTask $getSizePhotoTask
     */
    public function __construct(GetSizePhotoTask $getSizePhotoTask)
    {
        $this->getSizePhotoTask = $getSizePhotoTask;
    }

    /**
     * @param array $data
     * @return array|mixed
     */
    public function run(array $data)
    {
        return $this->getSizePhotoTask->run($data);
    }
}