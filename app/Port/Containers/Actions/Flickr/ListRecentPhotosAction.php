<?php

namespace App\Port\Containers\Actions\Flickr;

use App\Port\Containers\Tasks\Flickr\ListRecentPhotosTask;
use App\Port\Containers\Tasks\Flickr\ResponseRecentPhotosTask;
use App\Port\Contracts\Actions\Action;

/**
 * Class ListRecentPhotosAction
 * @package App\Port\Containers\Actions\Flickr
 */
class ListRecentPhotosAction implements Action
{
    /**
     * @var ListRecentPhotosTask
     */
    private $listRecentPhotosTask;

    /**
     * @var ResponseRecentPhotosTask
     */
    private $responseRecentPhotosTask;

    /**
     * ListRecentPhotosAction constructor.
     * @param ListRecentPhotosTask $listRecentPhotosTask
     * @param ResponseRecentPhotosTask $responseRecentPhotosTask
     */
    public function __construct(
        ListRecentPhotosTask $listRecentPhotosTask,
        ResponseRecentPhotosTask $responseRecentPhotosTask
    ) {
        $this->listRecentPhotosTask = $listRecentPhotosTask;
        $this->responseRecentPhotosTask = $responseRecentPhotosTask;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function run(array $data): array
    {
        return $this->responseRecentPhotosTask->run(
            $this->listRecentPhotosTask->run($data)
        );
    }
}