<?php

namespace App\Port\Contracts\Actions;

/**
 * Interface Action
 * @package App\Port\Contracts\Actions
 */
interface Action
{
    public function run(array $data);
}